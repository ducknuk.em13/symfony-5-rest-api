<?php


namespace App\Controller;


use App\Entity\Analytics;
use App\Entity\Client;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateClient
{

    /**
     * @Route("/api/create/client", name="create_client")
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function createClient(EntityManagerInterface $entityManager, Request $request): JsonResponse
    {
        $client = new Client();
        $analytics = new Analytics();
        $entityManager->persist($client);
        $entityManager->persist($analytics);

        $client->setAnalytics($analytics);

        $allPassed = true;
        foreach ($request->request->all() as $item){
            if ( $item === '' ){
                $allPassed = false;
            }
        }
        if ( $allPassed ){
            $client->setName($request->request->get('name'));
            $client->setPhone($request->request->get('phone'));
            $client->setEmail($request->request->get('email'));
            $client->setExtraInfo($request->request->get('extraInfo'));
            $analytics->setUtmContent($request->request->get('utmContent'));
            $analytics->setUtmCampaign($request->request->get('utmCampaign'));
            $analytics->setUtmMedium($request->request->get('utmMedium'));
            $analytics->setUtmSource($request->request->get('utmSource'));
            $analytics->setUtmTerm($request->request->get('utmTerm'));
        }
        try {
            $entityManager->flush();
            return new JsonResponse('Success');
        } catch (Exception $e) {
            return new JsonResponse('Error: ' . $e);
        }

    }
}