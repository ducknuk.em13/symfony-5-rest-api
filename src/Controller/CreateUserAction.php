<?php


namespace App\Controller;


use App\Security\UserFetcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/users/me", methods={"GET"})
 */
class CreateUserAction
{
    /**
     * @var UserFetcherInterface
     */
    private $userFetcher;

    public function __construct(UserFetcherInterface $userFetcher)
    {
        $this->userFetcher = $userFetcher;
    }

    public function __invoke()
    {
        $user = $this->userFetcher->getAuthUser();

        return new JsonResponse([
            'id' => $user->getId(),
            'email' => $user->getEmail(),
        ]);
    }
}