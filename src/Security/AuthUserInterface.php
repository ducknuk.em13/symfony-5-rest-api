<?php


namespace App\Security;


use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

interface AuthUserInterface extends PasswordAuthenticatedUserInterface
{
    public function getId(): string;
    public function getEmail(): string;
}