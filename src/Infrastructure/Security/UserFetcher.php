<?php


namespace App\Infrastructure\Security;


use App\Security\AuthUserInterface;
use App\Security\UserFetcherInterface;
use Symfony\Component\Security\Core\Security;
use Webmozart\Assert\Assert;

class UserFetcher implements UserFetcherInterface
{

    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function getAuthUser(): AuthUserInterface
    {
        /** @var AuthUserInterface $user*/
        $user = $this->security->getUser();

        Assert::notNull($user, 'User not found.');
        Assert::isInstanceOf($user, AuthUserInterface::class, sprintf('Invalid user type %s', \get_class($user)));

        return $user;
    }
}