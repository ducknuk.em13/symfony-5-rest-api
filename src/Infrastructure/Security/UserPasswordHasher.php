<?php


namespace App\Infrastructure\Security;


use App\Entity\User;
use App\Security\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface as BaseUserPasswordHasherInterface;

class UserPasswordHasher implements UserPasswordHasherInterface
{

    /**
     * @var BaseUserPasswordHasherInterface
     */
    private $passwordHasher;

    public function __construct(BaseUserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function hash(User $user, string $password): string
    {
        return $this->passwordHasher->hashPassword($user, $password);
    }
}