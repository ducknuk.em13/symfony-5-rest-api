<?php


namespace App\Factory;


use App\Entity\User;
use App\Security\UserPasswordHasherInterface;


class UserFactory
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function create(string $email, string $password): User
    {
        $user = new User();
        $user->setEmail($email);
        $user->setPassword($password, $this->passwordHasher);

        return  $user;
    }
}