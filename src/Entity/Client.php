<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Client
 * @ApiResource(
 * attributes={
 *      "normalization_context"={"groups"={"client:read"}},
 *      "pagination_items_per_page"=5,
 *      "enable_max_depth"="true",
 *      "order" = { "name" : "DESC"},
 *  }
 * )
 * @ApiFilter(
 *     OrderFilter::class, properties={
 *          "analytics.utm_source",
 *          "analytics.utm_medium",
 *     }
 * )
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @Groups({
     *     "client:read",
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "client:read",
     * })
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "client:read",
     * })"
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "client:read",
     * })
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({
     *     "client:read",
     * })
     */
    private $extra_info;

    /**
     * @ORM\OneToOne(targetEntity=Analytics::class, cascade={"persist", "remove"})
     *  @Groups({
     *     "client:read",
     * })
     */
    private $analytics;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getExtraInfo(): ?string
    {
        return $this->extra_info;
    }

    public function setExtraInfo(?string $extra_info): self
    {
        $this->extra_info = $extra_info;

        return $this;
    }

    public function getAnalytics(): ?Analytics
    {
        return $this->analytics;
    }

    public function setAnalytics(?Analytics $analytics): self
    {
        $this->analytics = $analytics;

        return $this;
    }
}
