<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AnalyticsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Analytics
 * @ApiResource()
 * @ORM\Entity(repositoryClass=AnalyticsRepository::class)
 */
class Analytics
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *  @Groups({
     *     "client:read"
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Groups({
     *     "client:read",
     * })
     */
    private $utm_source;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Groups({
     *     "client:read",
     * })
     */
    private $utm_medium;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Groups({
     *     "client:read",
     * })
     */
    private $utm_campaign;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Groups({
     *     "client:read",
     * })
     */
    private $utm_content;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Groups({
     *     "client:read",
     * })
     */
    private $utm_term;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUtmSource(): ?string
    {
        return $this->utm_source;
    }

    public function setUtmSource(string $utm_source): self
    {
        $this->utm_source = $utm_source;

        return $this;
    }

    public function getUtmMedium(): ?string
    {
        return $this->utm_medium;
    }

    public function setUtmMedium(string $utm_medium): self
    {
        $this->utm_medium = $utm_medium;

        return $this;
    }

    public function getUtmCampaign(): ?string
    {
        return $this->utm_campaign;
    }

    public function setUtmCampaign(string $utm_campaign): self
    {
        $this->utm_campaign = $utm_campaign;

        return $this;
    }

    public function getUtmContent(): ?string
    {
        return $this->utm_content;
    }

    public function setUtmContent(string $utm_content): self
    {
        $this->utm_content = $utm_content;

        return $this;
    }

    public function getUtmTerm(): ?string
    {
        return $this->utm_term;
    }

    public function setUtmTerm(string $utm_term): self
    {
        $this->utm_term = $utm_term;

        return $this;
    }
}
