# symfony-5-rest-api

Для разворачиваяни данного приложения требуется:
```
    composer install
```

Для запуска: 

```
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate
    php bin/console app:users:create-user   для создания пользователя для выпуска токена
    symfony server:start
```
